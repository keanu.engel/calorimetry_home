"""
Praktikum Digitalisierung

Kaloriemetrie - Küchentischversuch

@author: Keanu Engel
"""

from functions import m_json
from functions import m_pck

# Pfad zum Setup des Versuchs 
path = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"

# Metadaten abrufen und Seriennummerhinzufügen
metadata = m_json.get_metadata_from_setup(path)
metadata = m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadata)

# Messen der Temperaturdaten
data = m_pck.get_meas_data_calorimetry(metadata)

# Abspeichern der gesammelten Daten in den entsprechenden Ordner
m_pck.logging_calorimetry(data, metadata, '/home/pi/calorimetry_home/data/heat_capacity', '/home/pi/calorimetry_home/datasheets')
m_json.archiv_json('/home/pi/calorimetry_home', path, '/home/pi/calorimetry_home/data/heat_capacity/archiv')